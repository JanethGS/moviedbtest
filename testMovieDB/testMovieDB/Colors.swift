//
//  Colors.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import UIKit

extension UIColor {
    
    class var greenDark: UIColor {
        return UIColor(red: 25, green: 39, blue: 45, alpha: 1.0)
    }
    
    class var greenTextColor: UIColor {
        return UIColor(red: 98, green: 203, blue: 113, alpha: 1.0)
    }
}
