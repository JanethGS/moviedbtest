//
//  MoviesInteractor.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import UIKit

class MoviesInteractor: MoviesInteractorDelegate {
    
    var presenter: MoviesPresenterDelegate?
    
    func getCategories(){
        let categories = [ Categories(title: "Now Playing", url: "now_playing"),
                           Categories(title: "Popular", url: "popular"),
                           Categories(title: "Top Rated", url: "top_rated"),
                           Categories(title: "Upcoming", url: "upcoming")]
        presenter?.successListCategorias(categories)
    }
    
    
    func requestMoviesList(_ categoryTittle: String){
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(categoryTittle)?api_key=576fc4620c3c1eca2bf64bb72756bd24&language=es-MX&page=1")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else{
                self.presenter?.errorMovieList(error.debugDescription)
                return
            }
            guard let data = data else {
                self.presenter?.errorMovieList("No existen peliculas de la categoria seleccionada")
                return
            }
            do {
                let jsonResponse = try JSONDecoder().decode(MovieList.self, from: data)
                DispatchQueue.main.async {
                    self.presenter?.successMovieList(jsonResponse)
                }
            } catch {
                print("Error \(error.localizedDescription)")
            }
            
        }.resume()
    }
    
    
}
