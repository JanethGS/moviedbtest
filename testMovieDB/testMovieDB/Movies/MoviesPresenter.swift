//
//  MoviesPresenter.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import Foundation


class MoviesPresenter: MoviesPresenterDelegate {
   
    
    
    var view: MoviesViewDelegate?
    var interactor: MoviesInteractorDelegate?
    
    init(interface: MoviesViewDelegate, interactor: MoviesInteractorDelegate) {
        self.view = interface
        self.interactor = interactor
    }
    
    func getCategoriesList() {
        interactor?.getCategories()
    }
    
    func successListCategorias(_ categories: [Categories]) {
        view?.showCategories(categories)
    }
    
    func getMovieList(_ categorieTittle: String) {
        interactor?.requestMoviesList(categorieTittle)
    }
    
    func errorMovieList(_ msg: String) {
        view?.showErrorMovies(msg)
    }
    
    func successMovieList(_ listMovies: MovieList) {
        view?.showMovieList(listMovies)
    }
   
}
