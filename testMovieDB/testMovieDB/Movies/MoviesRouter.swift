//
//  MoviesRouter.swift
//  testMovieDB
//
//  Created by Invitado on 23/02/23.
//

import Foundation
import UIKit

class MoviesRouter: MoviesRouterDelegate {
        
   static func createModule() -> UIViewController {
        
        let view = MoviesViewController(nibName: "MoviesViewController", bundle: Bundle(for: MoviesViewController.self))
        let interactor = MoviesInteractor()
        let presenter = MoviesPresenter(interface: view, interactor: interactor)
        view.presenter = presenter
        interactor.presenter = presenter
        
        return view
    }

}
