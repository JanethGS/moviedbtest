//
//  MovieCell.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import Foundation
import UIKit

class MovieCell:UICollectionViewCell{
    @IBOutlet weak var tittleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var qualificationLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var iconMovie: UIImageView!
    @IBOutlet weak var favoriteBtn: UIButton!
   
    
    func configCell(){
        self.backgroundColor = UIColor.greenDark
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        self.tittleLbl.numberOfLines = 2
        self.tittleLbl.textColor = UIColor.greenTextColor
        self.qualificationLbl.textColor = UIColor.greenTextColor
        self.dateLbl.textColor = UIColor.greenTextColor
        self.descriptionLbl.textColor = UIColor.white
        
        
    }
}
