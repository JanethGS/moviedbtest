//
//  MoviesProtocols.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import Foundation

protocol MoviesViewDelegate: AnyObject {
    var presenter: MoviesPresenterDelegate? { get set }
    func showCategories(_ categories:[Categories])
    func showMovieList(_ movies: MovieList)
    func showErrorMovies(_ msg: String)
    
   
   

}
protocol MoviesPresenterDelegate: AnyObject {
    func getMovieList(_ categorieTittle: String)
    func errorMovieList(_ msg: String)
    func successMovieList(_ listMovies: MovieList)
    func getCategoriesList()
    func successListCategorias(_ categories:[Categories])

}
protocol MoviesInteractorDelegate: AnyObject {
    func getCategories()
    func requestMoviesList(_ categoryTittle: String)
}
protocol MoviesRouterDelegate: AnyObject {}
