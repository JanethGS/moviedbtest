//
//  MoviesViewController.swift
//  testMovieDB
//
//  Created by JanethGS on 23/02/23.
//

import UIKit
import Foundation
import MaterialDesignWidgets


class MoviesViewController: UIViewController {
    
    var presenter: MoviesPresenterDelegate?
    
    @IBOutlet weak var categoriesSegmented: MaterialSegmentedControl!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    private var arrayMovies:[Movies]? = []
    
    private var arrayCategories: [Categories] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configView()
    }
    
    
    private func configView(){
        self.title = "TV SHOWS"
        self.moviesCollectionView.delegate = self
        self.moviesCollectionView.dataSource = self
        let nibName = UINib(nibName: "MovieCell", bundle: Bundle(for: MoviesViewController.self))
        moviesCollectionView.register(nibName, forCellWithReuseIdentifier: "MovieCell")
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.presenter?.getCategoriesList()
        
    }
    
 
    private func setSegments(){
        for category in self.arrayCategories{
            let segment = MaterialButton(text: category.title, buttonStyle: .fill)
            //self.categoriesSegmented.segments.append(segment)
        }
    }
    
}


extension MoviesViewController: MoviesViewDelegate{
    func showMovieList(_ movies: MovieList) {
        self.arrayMovies = movies.results
        self.moviesCollectionView.reloadData()
    }
    
    func showErrorMovies(_ msg: String) {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCategories(_ categories: [Categories]) {
        self.arrayCategories = categories
        self.setSegments()
        self.presenter?.getMovieList(self.arrayCategories[0].url ?? "")
    }
    
    
}

extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrayMovies?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as? MovieCell {
            cell.configCell()
            cell.tittleLbl.text = arrayMovies?[indexPath.row].originalTitle
            cell.qualificationLbl.text =  (String(describing: arrayMovies?[indexPath.row].voteAverage ?? 0.0))
            cell.qualificationLbl.setIcon(imageName: "star")
            cell.descriptionLbl.text = arrayMovies?[indexPath.row].overview
            cell.dateLbl.text = arrayMovies?[indexPath.row].releaseDate
        
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let withCell = (collectionView.frame.width - 10)/2
        return  CGSize(width: withCell, height: (withCell * 2))
        
    }
    
}
