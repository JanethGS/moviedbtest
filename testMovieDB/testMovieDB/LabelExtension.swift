//
//  LabelExtension.swift
//  testMovieDB
//
//  Created by Invitado on 23/02/23.
//

import UIKit

extension UILabel{
    
    func setIcon(imageName: String)
        {
            
            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image = UIImage(systemName: imageName)?.withTintColor(.greenTextColor)
            

            let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
            let myString:NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
            myString.append(attachmentString)

            self.attributedText = myString
        }
}
