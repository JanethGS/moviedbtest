//
//  SesionValues.swift
//  testMovieDB
//
//  Created by JanethGS on 22/02/23.
//

import Foundation


class SesionValues {
    
    static let shared = SesionValues()
    
    var token: String?
    var usuario: String?
    
    
    func addInfoUser(token: String, usuario: String){
        self.token = token
        self.usuario = usuario
    }
    
    func resetLoginData(){
        self.token = nil
        self.usuario = nil
    }

}
