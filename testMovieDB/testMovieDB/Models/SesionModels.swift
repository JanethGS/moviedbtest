//
//  SesionModels.swift
//  testMovieDB
//
//  Created by JanethGS on 22/02/23.
//

import Foundation

struct Token: Codable {
    let success: Bool
    let expiresAt, requestToken: String

    enum CodingKeys: String, CodingKey {
        case expiresAt = "expires_at"
        case requestToken = "request_token"
        case success
        
    }
}

struct RequestUserToken: Codable {
    let username, password, requestToken: String

    enum CodingKeys: String, CodingKey {
        case username, password
        case requestToken = "request_token"
    }
}


