//
//  LoginRouter.swift
//  testMovieDB
//
//  Created by JanethGS on 22/02/23.
//

import UIKit

class LoginRouter: LoginRouterDelegate {
        
   static func createModule() -> UIViewController {
        
        let view = LoginViewController(nibName: "LoginViewController", bundle: Bundle(for: LoginViewController.self))
        let interactor =  LoginInteractor()
        let router = LoginRouter()
        let presenter = LoginPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
       interactor.presenter = presenter
        return view
    }
    
    func presentMoviesListView(from view:LoginViewDelegate){
        let moviesView =  MoviesRouter.createModule()
        if let newView = view as? UIViewController{
            newView.navigationController?.pushViewController(moviesView, animated: true)
        }
        
    }

}
