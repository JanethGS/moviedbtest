//
//  LoginViewController.swift
//  testMovieDB
//
//  Created by Janeth GS on 22/02/23.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var userTxt: UITextField!
    
    var presenter: LoginPresenterDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorLbl.text = ""
        self.activity.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func actionLogIn(_ sender: UIButton) {
        self.errorLbl.text = ""
        let user = userTxt.text ?? ""
        let pass = passTxt.text ?? ""
        if !user.isEmpty && !user.isEmpty{
            presenter?.validateInfo(usuario: user, codigo: pass)
        }else{
            errorLbl.text = "Porfavor ingrese usuario y contraseña."
        }
        
    }
}


extension LoginViewController: LoginViewDelegate{
    func showActivity() {
        self.activity.isHidden = false
        activity.startAnimating()
    }
    
    func hideActivity() {
        self.activity.isHidden = true
        activity.stopAnimating()
    }
    
    
    func showMovies() {
        presenter?.showMoviesView()
    }
    
    func showError(_ mensaje:  String) {
        errorLbl.text = mensaje
    }

}
