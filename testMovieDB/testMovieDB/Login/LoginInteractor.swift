//
//  LoginInteractor.swift
//  testMovieDB
//
//  Created by JanethGS on 17/02/23.
//

import Foundation

class LoginInteractor: LoginInteractorDelegate {
    var presenter: LoginPresenterDelegate?
    func logIn(usuario: String, codigo: String){
        
        self.createToken { token in
            let url = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=576fc4620c3c1eca2bf64bb72756bd24")
                    
            let model  = RequestUserToken(username: usuario, password: codigo, requestToken: token)
            guard let jsonData = try? JSONEncoder().encode(model) else {
                      print("Error: Trying to convert model to JSON data")
                      return
                  }
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
                URLSession.shared.dataTask(with: request) { data, response, error in
                    
                    guard error == nil else{
                        self.presenter?.errorLogin("")
                        return
                    }
                    guard let data = data else {
                        self.presenter?.errorLogin("ERROR: No se pudieron optener los datos.")
                        return
                    }
                    do {
                        let jsonResponse = try JSONDecoder().decode(Token.self, from: data)
                        DispatchQueue.main.async {
                            SesionValues.shared.addInfoUser(token: jsonResponse.requestToken, usuario: usuario)
                            self.presenter?.successLogin()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.presenter?.errorLogin("Usuario y contraseña invalidos.")
                            
                        }
                    }
                    
                }.resume()
        }
        
    }
    
    func createToken(completion: @escaping (_ token: String) -> Void){
        let url = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=576fc4620c3c1eca2bf64bb72756bd24")
                
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard error == nil else{
                    self.presenter?.errorLogin("")
                    return
                }
                guard let data = data else {
                    self.presenter?.errorLogin("")
                    return
                }

                do {
                    let jsonResponse = try JSONDecoder().decode(Token.self, from: data)
                    completion(jsonResponse.requestToken)
                } catch {
                    DispatchQueue.main.async {
                        self.presenter?.errorLogin(error.localizedDescription)
                    }
                }
                
            }.resume()
    }
    
}
