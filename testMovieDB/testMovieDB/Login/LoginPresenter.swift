//
//  LoginPresenter.swift
//  testMovieDB
//
//  Created by JanethGS on 22/02/23.
//

import Foundation

class LoginPresenter: LoginPresenterDelegate {
    
    
    var view: LoginViewDelegate?
    var interactor: LoginInteractorDelegate?
    var router: LoginRouterDelegate?

    init(interface: LoginViewDelegate, interactor: LoginInteractorDelegate, router:LoginRouterDelegate) {
       self.view = interface
       self.interactor = interactor
       self.router = router
   }
    
    func validateInfo(usuario: String, codigo: String){
        view?.showActivity()
        interactor?.logIn(usuario: usuario, codigo: codigo)
    }
    
    func successLogin(){
        view?.hideActivity()
        view?.showMovies()
    }
    
    func errorLogin(_ msg: String){
        view?.hideActivity()
        view?.showError(msg)
    }
    
    func showMoviesView(){
        router?.presentMoviesListView(from: view!)
    }
}
