//
//  LoginProtocols.swift
//  testMovieDB
//
//  Created by JanethGS on 22/02/23.
//

import UIKit

protocol LoginViewDelegate: AnyObject {
    var presenter: LoginPresenterDelegate? { get set }
    func showMovies()
    func showError(_ mensaje:  String)
    func showActivity()
    func hideActivity()
}
protocol LoginPresenterDelegate: AnyObject {
    func validateInfo(usuario: String, codigo: String)
    func successLogin()
    func errorLogin(_ msg: String)
    func showMoviesView()
    
}
protocol LoginInteractorDelegate: AnyObject {
    var presenter: LoginPresenterDelegate? { get set }
    func logIn(usuario: String, codigo: String)

}
protocol LoginRouterDelegate: AnyObject {
    func presentMoviesListView(from view:LoginViewDelegate)
}
